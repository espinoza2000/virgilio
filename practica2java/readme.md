**PRÁCTICA 2**

1.-Averigue para qué sirve la función “addEventListener”.y realice un ejemplo funcional en un archivo HTML con etiquetas “scrip”

R.- La función “addEventListener” es un método utilizado en JavaScript para adjuntar un evento a un elemento del DOM (Modelo de Objetos del Documento, por sus siglas en inglés). Permite definir una función que se ejecutará cuando ocurra un evento específico en el elemento seleccionado.

![](https://gitlab.com/espinoza2000/virgilio/-/raw/main/img/image%20(2).png?inline=false)



2.-Averigue y realice un ejemplo funcional de la creación, recorrido de posiciones y obtener  la longitud de un valor en JS.

R,-Realizamos un  ejemplo, creamos una variable `texto` que contiene la cadena de texto "practica". Luego, utilizamos la propiedad `length` para obtener la longitud de la cadena, es decir, el número de caracteres que contiene.

 ![](https://gitlab.com/espinoza2000/virgilio/-/raw/main/img/iamagen2%20(2).png?inline=false)
**Mostramos en js**

 ![](https://gitlab.com/espinoza2000/virgilio/-/raw/main/img/imagen3%20(2).png?inline=false)

 
 3.-Averigue y realice ejemplos funcionales de los tipos de funciones que hay en JS

 ![](https://gitlab.com/espinoza2000/virgilio/-/raw/main/img/imagen4%20(2).png?inline=false)
 
4.-Analice el siguiente código y encuentre el problema ;
![](https://gitlab.com/espinoza2000/virgilio/-/raw/main/img/imagen5%20(2).png?inline=false)

 El siguiente código necesita una función (); y como también el getsElementById se escribia getElementByld


**5**.-Cual es la diferencia entre JavaScript y TypeScript ; y crea una variable ,que almacene un número y luego hacer que se muestre en la consola en los dos lenguajes Para ello crear un archivo ts y js.

R.-JavaScript y TypeScript son dos lenguajes de programación que se utilizan comúnmente en el desarrollo web su Diferencia entre JavaScript y TypeScript:

**JavaScript** es un lenguaje de programación de alto nivel interpretado por los navegadores web. Es un lenguaje de scripting versátil que se utiliza para agregar interactividad y funcionalidad a las páginas web. JavaScript es dinámico y flexible, lo que significa que no se requiere una definición explícita de tipos de datos.

**TypeScript**, por otro lado, es un superconjunto de JavaScript. Agrega características adicionales al lenguaje, como la tipificación estática. TypeScript permite definir tipos de datos explícitos y proporciona herramientas de desarrollo más sólidas, como verificación estática de tipos y autocompletado, lo que facilita la detección de errores en etapas tempranas de desarrollo.

**JavaScript** 

![](https://gitlab.com/espinoza2000/virgilio/-/raw/main/img/imagen7%20(2).png?inline=false)

console
![](https://gitlab.com/espinoza2000/virgilio/-/raw/main/img/imagen9%20(2).png?inline=false)

**TypeScript**
![](https://gitlab.com/espinoza2000/virgilio/-/raw/main/img/imagen10%20(2).png?inline=false)
 
